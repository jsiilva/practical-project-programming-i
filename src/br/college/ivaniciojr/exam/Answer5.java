/*
 *  
 *  URSA - Univesidade RS�
 *  Trabalho pr�tico da disciplina de Programa��o I
 *  Docente: Dr. Felipe Francisco
 *  Discente: Ivanicio J�nior
 *  
 *  @referencias: http://www.ensineflex.inforgeneses.inf.br/public/uploads/067/noticias/4615/4328.pdf
 *   
*/
package br.college.ivaniciojr.exam;

import java.util.Scanner;

public class Answer5 {

	public static void main(String[] args) {
		
		Scanner in = new Scanner(System.in);
		int numero;
		
		// l� a entrada do usu�rio
		System.out.println("Digite um n�mero de pelo menos 3 caracteres: ");
		numero = in.nextInt();
		
		/* 
		 * Aqui converto o n�mero para o objeto String, para contar a quantidade de caracteres 
		 * mediante � API do Java utilizando o m�todo length()
		*/
		String numeroEmString = Integer.toString(numero);
		
		// Caso a quantidade de n�meros seja menor que 2, n�o tem gra�a exibi-los um em linha...
		if (numeroEmString.length() < 2) {
			System.out.println("\nDigite um n�mero maior ou igual a 3!");
		} else {
			// ... Mas caso inverso, crio um vetor de caracteres e converto o numero em String que cria um array de chars
			char[] digitos = String.valueOf(numero).toCharArray();
			
			// Percorro todos os elementos. Para todos os d�gitos em caractere, armazeno em "d�gito"
			for (char digito: digitos){
				System.out.println(digito);
			}
		}		
		
	}

}
