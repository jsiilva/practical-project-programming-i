/*
 *  
 *  URSA - Univesidade RS�
 *  Trabalho pr�tico da disciplina de Programa��o I
 *  Docente: Dr. Felipe Francisco
 *  Discente: Ivanicio J�nior
 *     
*/
package br.college.ivaniciojr.exam;

import java.util.Scanner;

public class Answer11 {

	public static void main(String[] args) {
		
		Scanner in = new Scanner(System.in);
		float[][] a;
		float[][] b;
		float[][] c;
		int i, j, y;
		int dimensao_total;
		
		System.out.println("Digite uma dimens�o global para todas as dimens�es do vetor: ");
		dimensao_total = in.nextInt();
		
		a = new float[dimensao_total][dimensao_total];
		b = new float[dimensao_total][dimensao_total];
		c = new float[dimensao_total][dimensao_total];
		
		System.out.println("Alimente o vetor de A: ");
		for (i = 0; i < a.length; i++) {
			for (j = 0; j < a.length; j++) {
				a[i][j] = in.nextInt();
			}
		}

		System.out.println("\nAlimente o vetor de B: ");
		for (i = 0; i < b.length; i++) {
			for (j = 0; j < b.length; j++) {
				b[i][j] = in.nextInt();
			}
		}		
		
		for (i = 0; i < a[0].length; i++) {
			for (j = 0; j < b[0].length; j++) {
				c[i][j] = 0;
				for (y = 0; y < a[0].length; ) {
					c[i][j] = c[i][j] + a[i][j] * b[i][j];
				}
			}
		}
		
		System.out.println("Resultado da Matriz A x B: ");
		for (i = 0; i < a[0].length; i++) {
			for (j = 0; j < b[0].length; j++) {
				System.out.println(c[i][j]+"\n");
			}
		}
	}

}
