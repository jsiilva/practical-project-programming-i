/*
 *  
 *  URSA - Univesidade RS�
 *  Trabalho pr�tico da disciplina de Programa��o I
 *  Docente: Dr. Felipe Francisco
 *  Discente: Ivanicio J�nior
 *   
*/

package br.college.ivaniciojr.exam;

import java.util.Scanner;

public class Answer6 {

	/* 
	 * 
	 * Vari�veis de controle da classe
	 * 
	 * @i: iterador global (tem seu valor distinto dentro de cada escopo de bloco);
	 * @count_a, count_b: armazenam, respectivamente, os tamanhos de cada vetor. Isso evita ficar recalculando toda vez que passar 
	 * na verifica��o do bloco "For". Dessa forma, calculamos o tamanho apenas uma vez e armazenamos na mem�ria, logo, somos felizes;
	 * @produto: armazena o produto vetorial;
	 * 
	*/
	private static Scanner in = new Scanner(System.in);
	private static int i;
	private static int count_a, count_b;
	private static int produto;
	
	public static void main(String[] args) {
		
		int[] vetor_a, vetor_b;
		int elementos_a, elementos_b;
		
		// L� a quantidade de espa�os que queremos nos vetores
		System.out.println("Quantos elementos no Vetor A?");
		elementos_a = in.nextInt();
		
		System.out.println("\nQuantos elementos no Vetor B?");
		elementos_b = in.nextInt();
		
		// Aloca os espa�os para os vetores na mem�ria
		vetor_a = new int[elementos_a];
		vetor_b = new int[elementos_b];
		
		// Faz a contagem �nica do tamanho de cada vetor
		count_a = vetor_a.length;
		count_b = vetor_b.length;
		
		// Verifica se h� inconsist�ncia nos tamanhos dos vetores. Um n�o pode ser menor ou maior que o outro 
		if (count_a < count_b || count_b < count_a) {
			System.out.println("\nN�o � poss�vel calcular o produto entre os vetores.\nH� inconsist�ncia de tamanho entre os mesmos!");
		} else {
			// Alimenta cada �ndice dos vetores com valores desejados
			System.out.println("\nOK! Agora digite a sequ�ncia de "+elementos_a+" n�meros para o Vetor A: ");
			for (i = 0; i < count_a; i++) {
				vetor_a[i] = in.nextInt();
			}

			// Alimenta cada �ndice dos vetores com valores desejados�
			System.out.println("\nCERTO! Agora digite a sequ�ncia de "+elementos_b+" n�meros para o Vetor B: ");
			for (i = 0; i < count_b; i++) {
				vetor_b[i] = in.nextInt();
			}

			// E, por fim, calcula o produto vetorial
			produtoVetorial(vetor_a, vetor_b);
		}
	}

	/* 
	 * 
	 * M�todo para calcular o Produto Vetorial
	 * 
	*/	
	public static void produtoVetorial (int[] a, int[] b) {
		for (i = 0; i < count_a; i++) {
			produto = a[i] * b[i];
			System.out.println(a[i]+" * "+b[i]+" = "+produto);
		}
	}
	
}
