/*
 *  
 *  URSA - Univesidade RS�
 *  Trabalho pr�tico da disciplina de Programa��o I
 *  Docente: Dr. Felipe Francisco
 *  Discente: Ivanicio J�nior
 *   
*/
package br.college.ivaniciojr.exam;

import java.util.Scanner;

public class Answer1 {

	public static void main(String[] args) {
		
		Scanner in = new Scanner(System.in);
		int numero, soma = 0, i;
		
		System.out.println("Digite um valor inteiro positivo: ");
		numero = in.nextInt();
		
		// verifica se o n�mero n�o � negativo
		if (numero > 0) {
			// Faz o "fatorial" da soma. Hehehe
			for (i = 1; i <= numero; i++) {
				soma += i;
			}
			System.out.println("\nA soma dos primeiros "+numero+" n�meros positivos � "+soma);
		} else {
			System.out.println("ERRO! Digite um n�mero INTEIRO POSITIVO!");
		}
	}

}
