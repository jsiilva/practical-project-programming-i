/*
 *  
 *  URSA - Univesidade RS�
 *  Trabalho pr�tico da disciplina de Programa��o I
 *  Docente: Dr. Felipe Francisco
 *  Discente: Ivanicio J�nior
 *     
*/
package br.college.ivaniciojr.exam;

import java.util.Scanner;

public class Answer7 {

	public static void main(String[] args) {
		
		Scanner in = new Scanner(System.in);
		String valor;
		int aberto = 0, fechado = 0;
		
		// l� a entrada no formato da express�o (ex: "((())")
		System.out.println("Digite uma express�o para verificar o balanceamento entre par�nteses: ");
		valor = in.next();
		
		// Quebro a string em um Array de carateres
		char[] parenteses = String.valueOf(valor).toCharArray();
		
		// Percorro todos os caracteres da string e passo um a um mediante � vari�vel "parentese"
		for (char parentese: parenteses) {
			// Ent�o, para cada parentese '(' aberto,  incremento a vari�vel "aberto"
			if (parentese == '(') {
				aberto++;
			}
			// E, para cada parentese ')' fechado, incremento a vari�vel "fechado"
			if (parentese == ')') {
				fechado++;
				if (fechado > aberto)
					break;
			}
		}
		
		// No final das contas, se o n�mero de par�nteses abertos for igual ao n�mero de par�nteses fechados, h� balanceamento.
		if (aberto == fechado)
			System.out.println("\nOs par�nteses est�o balanceados!");
		else
			System.out.println("\nOs par�nteses N�O est�o balanceados!");
		
		in.close();
	}

}
