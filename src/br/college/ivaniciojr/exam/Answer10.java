/*
 *  
 *  URSA - Univesidade RS�
 *  Trabalho pr�tico da disciplina de Programa��o I
 *  Docente: Dr. Felipe Francisco
 *  Discente: Ivanicio J�nior
 *     
*/
package br.college.ivaniciojr.exam;

import java.util.Scanner;

public class Answer10 {

	public static void main(String[] args) {
		
		Scanner in = new Scanner(System.in);		
		int[][] x = new int[2][2];
		int[][] y = new int[2][2];
		
		System.out.println("Vetor A:");
		for (int i = 0; i < x.length; i++) {
			for (int j = 0; j < x.length; j++) {
				System.out.println("\nDigite o elemento da linha "+i+" e coluna "+j);
				x[i][j] = in.nextInt();
			}
		}
		
		System.out.println("Vetor B:");
		for (int i = 0; i < y.length; i++) {
			for (int j = 0; j < y.length; j++) {
				System.out.println("\nDigite o elemento da linha "+i+" e coluna "+j);
				y[i][j] = in.nextInt();
			}
		}
		
		System.out.println("Vetor resultante: \n");
		System.out.println(calculaProduto(x, y));
	}
	
	public static int[][] calculaProduto (int[][] a, int[][] b){
		
		// Em caso de inconsist�ncia entre os vatores, retorno uma exception logo de cara. 
		// J� que o retorno da fun��o espera ser um vetor multidimensional, a solu��o encontrada foi essa.
		if (a.length != b.length)
			throw new RuntimeException("H� inconsist�ncia de dimens�es nas matrizes!");
	
		// aloco cada dimens�o do vetor resultado simplesmente com o seu tamanho atual de cada vetor passado no par�metro
		int[][] resultado = new int[a.length][b[0].length];
		int i, j, y;

		/*
		 * Percorro parte por parte do meu vetor.
		 * 
		 * no iterador "a", percorro o vetor de a (do argumento) por inteiro, 
		 * a� dentro desse loop principal, percorro somente as linhas em zero dos vetores de a e b;
		 * em resultado, armazeno o inverso das linhas e colunas de a e b. 
		 * */
	
		for (i = 0; i < a.length; i++) {
			for (j = 0; j < b[0].length; j++) {
				for (y = 0; y < a[0].length; y++) {
					resultado[i][j] = (a[i][y] * b[y][j]);
				}
			}
		}
		
		return resultado;
	}
}
