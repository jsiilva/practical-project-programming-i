/*
 *  
 *  URSA - Univesidade RS�
 *  Trabalho pr�tico da disciplina de Programa��o I
 *  Docente: Dr. Felipe Francisco
 *  Dicente: Ivanicio J�nior
 *   
*/
package br.college.ivaniciojr.exam;

import java.util.Scanner;

public class Answer3 {

	private static Scanner in = new Scanner(System.in);
	
	public static void main(String[] args) {
		
		int valor, resultado;
		
		// l� o valor
		System.out.println("Digite um n�mero inteiro positivo: ");
		valor = in.nextInt();
		
		// calcula o valor
		resultado = fatorial(valor);
	
		if (resultado != -1)
			System.out.println("\n"+valor+"! = "+resultado);
		else
			System.out.println("\nDigite um n�mero inteiro positivo!");
	}
	
	/*
	 * 
	 * M�todo Fatorial
	 * 
	 * C�lcula o produto do decremento de um n�mero
	 * 
	 * */
	public static int fatorial (int numero) {
		int fatorial = 1;
		
		// Caso o n�mero n�o seja negativo...
		if (!(numero < 0)) {
			/* 
			 * Calcula o produto do n�mero, enquanto o n�mero passado pelo argumento da fun��o for maior ou igual a 2; 
			 * N�o necessita ir at� o zero, pois qualquer coisa por zero, � zero. E qualquer coisa por um, � ele mesmo.
			 *
			*/
			while (numero >= 2) {
				fatorial *= numero;
				numero--;
			}
			return fatorial;
		}
		
		/*
		 *  OBS:
		 *  N�o sou f� de aninhar express�es if e else.
		 *  
		 *  Gosto de inverter o range da verifica��o for�ando o retorno ser verdadeiro logo dentro do if, 
		 *  pois se a execu��o entrar naquele bloco, faz o que t� no bloco e j� retorna o resultado, matando o resto do processo. 
		 *  Por�m, se a condi��o for falsa, prossegue e apenas retorna o -1.  
		*/ 
		
		// Caso seja, retorna -1 como sinaliza��o de erro
		return -1;
	}
}
