/*
 *  
 *  URSA - Univesidade RS�
 *  Trabalho pr�tico da disciplina de Programa��o I
 *  Docente: Dr. Felipe Francisco
 *  Discente: Ivanicio J�nior
 *     
*/
package br.college.ivaniciojr.exam;

import java.util.Scanner;

public class Answer2 {

	public static void main(String[] args) {
		
		Scanner in = new Scanner(System.in);
		
		/*
		 * 
		 * Vari�veis de controle
		 * 
		 * i: iterador;
		 * dia: o dia da venda;
		 * diaMaiorVenda: Armazena o de maior lucro
		 * elementos: respons�vel por alocar o n�mero de itens do vetor
		 * 
		 * produtos_vendidos[]: array q armazena os dias das vendas
		 * */
		int i, dia = 0, diaMaiorVenda = 0, elementos;
		float[] produtos_vendidos;
		
		System.out.println("Quantos dias de vendas deseja registrar?");
		elementos = in.nextInt();
		
		// Aloca o produto na mem�ria
		produtos_vendidos = new float[elementos];
		// define uma vari�vel auxiliar "m�ximo" para armazenar sempre o primeiro item do array
		float max = produtos_vendidos[0];
		
		// Percorre todos os elementos do array
		for (i = 0; i < produtos_vendidos.length; i++) {
			// incrementa dia. (para fins de exibi��o apenas)
			dia++;
			if (dia == 1)
				System.out.println("\nAgora digite o valor total de vendas do dia "+dia+"� dia:");
			else 
				System.out.println("\nOK! Agora digite o valor total do "+dia+"� dia:");
			
			// aloca os valores em cada �ndice corrente no vetor
			produtos_vendidos[i] = in.nextFloat();			
		}
		
		// percorre novamente os elementos do array, s� que dessa vez para verificar o maior dia venda.
		for (i = 0; i < produtos_vendidos.length; i++) {
			// se, para cara cada pre�o informado, o mesmo for maior que o corrente na vari�vel auxliar "maximo (max)"
			if (produtos_vendidos[i] > max) {
				// max passa a receber o valor daquele maior valor e incrementa o dia da venda com o valor do �ndice + 1
				max = produtos_vendidos[i];
				diaMaiorVenda = i+1;
			}
		}
		
		// Imprime! :)
		System.out.println("\nRELAT�RIO");
		System.out.println("Intervalo de dias: "+dia+" dias");
		System.out.println("Maior venda realizada: R$"+max+" reais");
		System.out.println("A maior venda foi realizada no "+diaMaiorVenda+"� dia");
	}

}
