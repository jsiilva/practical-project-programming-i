/*
 *  
 *  URSA - Univesidade RS�
 *  Trabalho pr�tico da disciplina de Programa��o I
 *  Docente: Dr. Felipe Francisco
 *  Discente: Ivanicio J�nior
 *  
 *  @referencias: http://www.matematica.br/historia/nperfeitos.html
 *   
*/
package br.college.ivaniciojr.exam;

import java.util.Scanner;

public class Answer4 {

	public static void main(String[] args) {
		
		Scanner in = new Scanner(System.in);
		int numero = 0, soma = 0, divisor;
		
		// L� um n�mero inteiro
		System.out.println("Digite um n�mero inteiro positivo: ");
		numero = in.nextInt();
		
		// Cria a possibilidade de dividores at� o n�mero informado
		// Para cada resto da divis�o que for igual a zero, atribui � vari�vel soma o valor do iterador divisor + a soma
		for (divisor = 1; divisor < numero; divisor++) {
			if (numero % divisor == 0) {
				soma += divisor;
			}
		}
	
		// caso a soma seja igual ao n�mero, legal, ele � um n�mero perfeito
		if (numero == soma)
			System.out.println("\n"+numero+" � classificado como um n�mero perfeito!");
		else
			System.out.println("\n"+numero+" n�o � classificado como um n�mero perfeito!");
		
	}

}
