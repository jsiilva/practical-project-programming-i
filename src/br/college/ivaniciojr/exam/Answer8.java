/*
 *  
 *  URSA - Univesidade RS�
 *  Trabalho pr�tico da disciplina de Programa��o I
 *  Docente: Dr. Felipe Francisco
 *  Discente: Ivanicio J�nior
 *     
*/
package br.college.ivaniciojr.exam;

import java.util.Scanner;

public class Answer8 {

	public static void main(String[] args) {
		
		Scanner in = new Scanner(System.in);
		
		/*
		 * 
		 * Vari�veis de controle
		 * 
		 * i: iterador dos loops;
		 * elementos: quantidade de elementos para o vetor ser alocado din�micamente na mem�ria
		 * j: contador auxiliar
		 * count:  
		 * */
		int i, elementos, j, vezes;
		float[] vetor;
		
		// l� a quantidade de espa�os do vetor
		System.out.println("Digite o n�mero de elementos:");
		elementos = in.nextInt();
		
		// Aloca o vetor principal na mem�ria
		vetor = new float[elementos];
		
		// L� a sequ�ncia de valores
		System.out.println("\nDigite uma sequ�ncia de valores:");
		for (i = 0; i < elementos; i++) {
			vetor[i] = in.nextFloat();
		}
		
		// percorre todo o vetor principal
		for(i = 0; i < elementos; i++) {
			// a vari�vel vezes inicia, por padr�o, com 1
			vezes = 1;
			// o iterador "j" inicia com o iterador i+1. A busca ser� feita sempre no pr�ximo �ndice do array auxiliar. (s� funcionou assim)
			j = i+1;
			
			while (j < elementos) {
				// para cada elemento do vetor[i+1] que for diferente de vetor[i].
				// Ou seja, os n�meros n�o s�o iguais. Incrementa o iterador j para continuar "fu�ando" no vetor.
				if (vetor[j] != vetor[i]) {
					j++;
				} else {
					// se for igual, incrementa a vari�vel "vezes" e tira o elemento
					// por fim, o vetor no �ndice de i+1 recebe o valor de elementos
					vezes++;
					elementos--;
					vetor[j] = vetor[elementos];
				}
			}
			
			// Imprime! :)
			System.out.println(vetor[i]+" ocorre "+vezes+" veze(s)");
		}
		

	}
}
	